﻿using System.Web.Mvc;
using System.Web.Routing;

namespace BookStore.Controllers
{
    public class MyClassController : IController
    {
        public void Execute(RequestContext requestContext)
        {
            string ip = requestContext.HttpContext.Request.UserHostAddress;
            var response = requestContext.HttpContext.Response;
            response.Write("<h2>Ваш local IP-адрес: " + ip + "</h2>");
        }
    }
}