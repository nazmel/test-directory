﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookStore.Controllers
{
    public class ContextController : Controller
    {
        // GET: Context
        public ActionResult Index()
        {
            return View();
        }

        public string GetContext()
        {
            HttpContext.Response.Write("Data About request\n");
            
            string browser = HttpContext.Request.Browser.Browser;
            string user_agent = HttpContext.Request.UserAgent;
            string url = HttpContext.Request.RawUrl;
            string ip = HttpContext.Request.UserHostAddress;
            string referer = HttpContext.Request.UrlReferrer == null ? "" : HttpContext.Request.UrlReferrer.AbsoluteUri;

            return $"<p>Browser: {browser}; <br/>UserAgent: {user_agent} <br/>" +
                $"Url: {url}<br/>" +
                $"referer: {referer};<br/>" +
                $"IP: {ip}.";
                    
        }
    }
}