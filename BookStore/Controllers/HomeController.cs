﻿using BookStore.Models;
using BookStore.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace BookStore.Controllers
{
    public class HomeController : Controller
    {
        // создаем контекст данных
        BookContext db = new BookContext();

        public ActionResult Index()
        {
            // получаем из бд все объекты Book
            IEnumerable<Book> books = db.Books.ToList();
            // передаем все объекты в динамическое свойство Books в ViewBag
            ViewBag.Books = books;
            // возвращаем представление
            return View();
        }

        /*
         *Async method is possible in EF6
        public async Task<ActionResult> BookList()
        {
            
            IEnumerable<Book> books = await (db.Books).ToListAsync();

            ViewBag.Books = books;
            
            return View("Index");
        }*/

        [HttpGet]
        public ActionResult Buy(int id)
        {
            ViewBag.BookId = id;
            return View();
        }

        [HttpPost]
        public string Buy(Purchase purchase)
        {
            purchase.Date= DateTime.Now;
            db.Purchases.Add(purchase);
            db.SaveChanges();

            return $"Дякуємо {purchase.Person} за покупку";
        }

        public ActionResult GetHtml()
        {
            return new HtmlResult("<h2>Hello world!</h2>");
        }



        // переадресація
        public ActionResult GetVoid(int id)
        {
            if(id > 3)
            {
                //return RedirectToRoute(new { controller = "Home", action = "Contact" });

                return new HttpUnauthorizedResult();
            }
            return View("Buy");
               
        }

        public ActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";
            return View();
        }

        [HttpGet]
        public ActionResult CreateBook()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateBook(Book book)
        {
            db.Books.Add(book);
            db.SaveChanges();
            return RedirectToAction("Index");
        }


        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Book book)
        {
            db.Books.Add(book);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        //public ActionResult Delete(int id)
        //{
        //    Book b = db.Books.Find(id);
        //    if(b != null)
        //    {
        //        db.Books.Remove(b);
        //        db.SaveChanges();
        //    }

        //    return RedirectToAction("Index");
        //}

         
       [HttpGet]
       public ActionResult Delete(int id)
        {
            Book b = db.Books.Find(id);
            if(b == null)
            {
                return HttpNotFound();
            }
            return View(b);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Book b = db.Books.Find(id);
            if(b == null)
            {
                return HttpNotFound();
            }
            db.Books.Remove(b);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if(id == null)
            {
                return HttpNotFound();
            }
            Book book = db.Books.Find(id);
            if(book != null)
            {
                return View(book);
            }
            return HttpNotFound();
        }

        [HttpPost]
        public ActionResult Edit(Book book)
        {
            db.Entry(book).State = EntityState.Modified;  //UPDATE
            db.SaveChanges();
            return RedirectToAction("Index");
        }


        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}