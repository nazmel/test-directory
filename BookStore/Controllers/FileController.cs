﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookStore.Controllers
{
    public class FileController : Controller
    {
        // GET: File
        public ActionResult Index()
        {
            return View();
        }



        // download the file
        public FilePathResult GetFile()
        {
            string file_path = Server.MapPath("~/Files/Jamala.mp4");
            //string file_type = "application/docx";

            // unique types
            string file_type = "application/octer-stream";
            string file_name = "Jamala.mp4";

            return File(file_path, file_type, file_name);
        }


        // this method is the same as previous
        public FileContentResult GetBytes()
        {
            string file_path = Server.MapPath("~/Files/Резюме.docx");
            byte[] mas = System.IO.File.ReadAllBytes(file_path);
            string file_type = "application/octer-stream";
            string file_name = "Rezume.docx";

            return File(mas, file_type, file_name);
        }

        // the same
        public FileStreamResult GetStream()
        {
            string file_path = Server.MapPath("~/Files/Резюме.docx");
            FileStream fs = new FileStream(file_path, FileMode.Open);
            string file_type = "application/octer-stream";
            string file_name = "Rezume.docx";

            return File(fs, file_type, file_name);
        }
    }
}